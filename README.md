# Züri Wie Neu - Sample Data

This project will serve a fake API with a subset of the data from the [Züri Wie Neu](https://www.zueriwieneu.ch/) project, available as [Open Data](https://data.stadt-zuerich.ch/dataset/geo_zueri_wie_neu).

## How to serve the data

First install the dependencies:

```
pnpm install
```

To serve the data, there is a `serve` script:

```
pnpm serve
```

The API will now be reachable on `http://localhost:3030`

### Resources

There are two resources available, `messages` and `service_codes`. To get all the service codes:

```bash
curl http://localhost:3030/service_codes
```

For the messages, it's best to use pagination to fetch the data. For example to get max 20 messages from page 5:

```bash
curl "http://localhost:3030/messages?_limit=20&_page=5"
```

You can also get one specific message by id:

```bash
curl "http://localhost:3030/messages/33900
```

There are also many ways to search the collection:

```bash
# Get all fixed messages:
curl http://localhost:3030/messages?properties.status=fixed%20-%20council
# Get all messages that are not fixed
curl http://localhost:3030/messages?properties.status_ne=fixed%20-%20council

# Find a text in all fields (fulltext search)

curl http://localhost:3030/messages?q=stören
```

Other queries are available and can be found in the [documentation for JSON server](https://github.com/typicode/json-server#routes)
